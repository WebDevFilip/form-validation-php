<?php
if (isset($_POST['submit'])) {
    $uname = ($_POST['uname']);
    $email = ($_POST['email']);
    $pword1 = ($_POST['pword1']);
    $pword2 = ($_POST['pword2']);
    if (preg_match('/[A-Z]/', $uname) && (preg_match('/[a-z]/', $uname))) {
        echo "Your username has been validated as " . ($_POST['uname']);
        echo "<br>";
    } else {
        echo "Your username must include at least one capital and lowercase letter";
        echo "<br>";
    }
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo "Email has been confirmed. All alerts will be sent to " . ($_POST['email']);
        echo "<br>";
    } else {
        echo "Your email is not valid. Check that you have been in the required characters.";
        echo "<br>";
    }
    if (strlen($pword1) < 8) {
        echo "Password must be at least 8 characters long.";
        echo "<br>";
    } else {
        echo "Password has met the length requirements.";
    }
    if  (!preg_match('/[A-Z]/', $pword1) && (!preg_match('/[a-z]/', $pword1))) {
        echo "Password must contain uppercase and lowercase letters.";
        echo "<br>";
    }  else {
        echo "Password has met the letter requirements.";
    }
    if (!preg_match('/'.preg_quote('^\'£$%^&*()}{@#~?><,@|-=-_+-¬', '/').'/', $pword1)) {
        echo "Password must contain at least one symbol.";
        echo "<br>";
    } else {
        echo "Password has met the symbol requirements.";
    }
    if ($_POST['pword1'] != $_POST['pword2']) {
        echo "Passwords did not match and could not be validated.";
    } else {
        echo "Passwords match.";
    }
}